import { StackScreenProps } from '@react-navigation/stack';
import React, { FunctionComponent, useCallback } from 'react';
import { Pressable } from 'react-native';
import FastImage from 'react-native-fast-image';
import { createImageProgress } from 'react-native-image-progress';
import GridFlatList from 'grid-flatlist-react-native';
import ProgressBar from 'react-native-progress/Bar';

import { useNavigation } from '@react-navigation/native';
import { GetImagesByDateResponse, NavigationProps, StackParamList } from '../../../types';
import { getImageUri } from '../../../helpers';

export type PhotoGridProps = StackScreenProps<StackParamList, 'PhotoGrid'>

const Image = createImageProgress(FastImage);

export const PhotoGrid: FunctionComponent<PhotoGridProps> = ({ route: { params } }) => {
  const imagesToRender = [...params];

  const { navigate } = useNavigation<NavigationProps>();

  const handleImagePress = useCallback((imageData: GetImagesByDateResponse) => {
    navigate('PhotoVisualizer', imageData);
  }, []);

  return (
    <GridFlatList
      data={imagesToRender}
      renderItem={(item: GetImagesByDateResponse) => (
        <Pressable onPress={() => handleImagePress(item)}>
          <Image
            indicator={ProgressBar.Pie}
            style={{ width: 120, height: 120 }}
            source={{ uri: getImageUri(item), cache: FastImage.cacheControl.cacheOnly }}
          />
        </Pressable>

      )}
      numColumns={3}
      keyExtractor={(item, index) => `${item.image}${index}`}
      accessibilityComponentType={undefined}
      accessibilityTraits={undefined}
    />
  );
};
