import axios from 'axios';
import { EPIC_GSFC_BASE_URL } from '@env';
import { GetAvailableDatesResponse, GetImagesByDateResponse } from '../types';

const ENHANCED_ROUTE = 'api/enhanced';
export const ARCHIVE_ROUTE = 'archive/enhanced/';

axios.defaults.baseURL = `${EPIC_GSFC_BASE_URL}${ENHANCED_ROUTE}`;

export const getAvailableDates = async (): Promise<GetAvailableDatesResponse[]> => {
  const { data } = await axios.get('/all');

  return data;
};

export const getImagesByDate = async (date: string):Promise<GetImagesByDateResponse[]> => {
  const { data } = await axios.get(`/date/${date}`);

  return data;
};
