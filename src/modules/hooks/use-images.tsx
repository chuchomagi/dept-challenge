import { useCallback, useState } from 'react';
import FastImage from 'react-native-fast-image';
import { EPIC_GSFC_BASE_URL } from '@env';
import { ARCHIVE_ROUTE, getImagesByDate } from '../../api';
import { GetImagesByDateResponse } from '../../types';

interface UseImages {
    imagesLoading: boolean
    error: boolean
    images: GetImagesByDateResponse[]
    fetchImagesDataByDate: (_date: string)=>void
}

export const useImages = (): UseImages => {
  const [imagesLoading, setImagesLoading] = useState(false);
  const [error, setError] = useState(false);

  const [images, setImages] = useState<GetImagesByDateResponse[]>([]);

  const fetchImagesDataByDate = useCallback((date: string) => {
    setImagesLoading(true);

    getImagesByDate(date).then((data) => {
      setImages(data);
      FastImage.preload(images.map(({ image }) => ({
        uri: `${EPIC_GSFC_BASE_URL}${ARCHIVE_ROUTE}${date.replaceAll('-', '/').slice(0, 10)}/png/${image}.png`,
      })));
      setImagesLoading(false);
    }).catch((e) => setError(true));
  }, []);

  return {
    fetchImagesDataByDate,
    imagesLoading,
    error,
    images,
  };
};
