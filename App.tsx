import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { useEffect } from 'react';

import SplashScreen from 'react-native-splash-screen';
import { Home } from './src/modules/screens/home';
import { PhotoGrid } from './src/modules/screens/photo-grid';
import { PhotoVisualizer } from './src/modules/screens/photo-visualizer';

const Stack = createNativeStackNavigator();

const App = () => {
  useEffect(() => { setTimeout(() => SplashScreen.hide(), 2000); });

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{ title: 'Dates' }} />
        <Stack.Screen name="PhotoGrid" component={PhotoGrid} options={{ title: 'Photo Grid' }} />
        <Stack.Screen name="PhotoVisualizer" component={PhotoVisualizer} options={{ title: 'Photo Visualizer', headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
