import React, { FunctionComponent } from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
} from 'react-native';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { useDates } from '../../hooks/use-dates';
import { DateRow } from '../../../components/date-row';
import { NavigationProps } from '../../../types';

const Separator = styled.View`
color: #ADADAD;
border-width: 0.5px;
opacity:0.3;
`;

export const Home: FunctionComponent = () => {
  const { navigate } = useNavigation<NavigationProps>();

  const {
    datesLoading,
    error,
    availableDates,
    fetchAvailableDates,
  } = useDates();

  if (datesLoading) {
    return (
      <ActivityIndicator size="large" />
    );
  }

  if (error) {
    Alert.alert('ERROR', 'Unable to fetch available dates, please try again later', [{ text: 'Retry' }], { onDismiss: fetchAvailableDates });
  }

  return (
    <FlatList
      data={availableDates}
      renderItem={({ item }) => (
        <DateRow onPress={(props) => navigate('PhotoGrid', props)} date={item.date} />
      )}
      ItemSeparatorComponent={Separator}
      keyExtractor={(item, index) => `${item.date} ${index}`}
    />

  );
};
