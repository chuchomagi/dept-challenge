import { useCallback, useEffect, useState } from 'react';
import { getAvailableDates } from '../../api';
import { GetAvailableDatesResponse } from '../../types';

interface UseDates {
  datesLoading: boolean
  error: boolean
  availableDates:GetAvailableDatesResponse[]
  fetchAvailableDates: ()=>void
}

export const useDates = ():UseDates => {
  const [datesLoading, setDatesLoading] = useState(false);

  const [error, setError] = useState(false);

  const [availableDates, setAvailableDates] = useState<GetAvailableDatesResponse[]>([]);

  const fetchAvailableDates = useCallback(() => {
    setDatesLoading(true);

    getAvailableDates().then((dates) => {
      setAvailableDates(dates);
    })
      .catch((e) => setError(true))
      .finally(() => setDatesLoading(false));
  }, []);

  useEffect(() => fetchAvailableDates(), []);

  return {
    datesLoading,
    error,
    availableDates,
    fetchAvailableDates,
  };
};
