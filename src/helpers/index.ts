import { EPIC_GSFC_BASE_URL } from '@env';
import { ARCHIVE_ROUTE } from '../api';
import { GetImagesByDateResponse } from '../types';

export const getImageUri = ({ date, image }: Pick<GetImagesByDateResponse, 'date' | 'image'>) => `${EPIC_GSFC_BASE_URL}${ARCHIVE_ROUTE}${date.replaceAll('-', '/').slice(0, 10)}/png/${image}.png`;
