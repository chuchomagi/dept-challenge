import React, { FunctionComponent } from 'react';
import styled from 'styled-components/native';

export interface ImageIndicatorProps {
    variant: 'gray' | 'yellow' | 'green'
}

const Circle = styled.View<ImageIndicatorProps>`
    border-radius: 20px;
    border-width: 0.5px;
    width: 12px;
    height: 12px;
    background-color: ${({ variant }) => variant};
`;

export const ImageIndicator:
FunctionComponent<ImageIndicatorProps> = ({ variant }) => (<Circle variant={variant} />);
