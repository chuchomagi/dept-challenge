import { StackNavigationProp } from '@react-navigation/stack';

export interface GetAvailableDatesResponse {
    date: string
}

export interface GetImagesByDateResponse {
    identifier: string
    image: string
    date: string
    caption: string
}

export type StackParamList = {
    Home: undefined
    PhotoGrid: GetImagesByDateResponse[]
    PhotoVisualizer: GetImagesByDateResponse
}

export type NavigationProps = StackNavigationProp<StackParamList>
