import { StackScreenProps } from '@react-navigation/stack';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { createImageProgress } from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import ImageView from 'react-native-image-viewing';

import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components/native';
import { NavigationProps, StackParamList } from '../../../types';
import { getImageUri } from '../../../helpers';

const Caption = styled.Text`
    color: white;
    margin: 20px;
`;

const Image = createImageProgress(ImageView);

type PhotoVisualizerProps = StackScreenProps<StackParamList, 'PhotoVisualizer'>

const BottomCaption:
FunctionComponent<{caption: string}> = ({ caption }) => <Caption>{caption}</Caption>;

export const PhotoVisualizer: FunctionComponent<PhotoVisualizerProps> = ({
  route:
    {
      params: { caption, image, date },
    },
}) => {
  const [visible, setIsVisible] = useState(true);

  const { goBack } = useNavigation<NavigationProps>();

  useEffect(() => {
    if (!visible) { goBack(); }
  }, [visible]);

  return (
    <View>
      <Image
        images={[{ uri: getImageUri({ date, image }) }]}
        imageIndex={0}
        visible={visible}
        indicator={ProgressBar.Pie}
        onRequestClose={() => {
          setIsVisible(false);
        }}
        FooterComponent={() => BottomCaption({ caption })}
      />
    </View>
  );
};
