import React, {
  FunctionComponent, useEffect, useMemo, useState,
} from 'react';
import styled from 'styled-components/native';
import { useImages } from '../modules/hooks/use-images';
import { GetAvailableDatesResponse, GetImagesByDateResponse } from '../types';
import { ImageIndicator, ImageIndicatorProps } from './indicators';

const RowContainer = styled.Pressable`
  flex-direction: row;
  margin: 5px;
  justify-content: space-between;
  align-items: center;
  height: 30px;
`;

const Date = styled.Text`
  font-size:18px;
`;

type DateRowProps = GetAvailableDatesResponse
& {onPress: (images: GetImagesByDateResponse[])=> void}

export const DateRow:
 FunctionComponent<DateRowProps> = ({ date, onPress }) => {
   const [fetchRequested, setFetchRequested] = useState(false);

   const {
     fetchImagesDataByDate, imagesLoading, images, error,
   } = useImages();

   useEffect(() => {
     setFetchRequested(true);
     fetchImagesDataByDate(date);
   }, []);

   const indicatorProps: ImageIndicatorProps = useMemo(() => {
     if (!fetchRequested || !images.length || error) return { variant: 'gray' };

     if (imagesLoading) return { variant: 'yellow' };

     return { variant: 'green' };
   }, [fetchRequested, images.length, error, imagesLoading]);

   return (
     <RowContainer onPress={() => onPress(images)}>
       <Date>{date}</Date>
       <ImageIndicator {...indicatorProps} />
     </RowContainer>
   );
 };
